import * as axios from 'axios';
// import { follow } from '../Redux/users-reducer';



//creo istanza dell axios
const instance = axios.create({
    withCredentials: true,
    baseURL:'https://social-network.samuraijs.com/api/1.0/',
    headers:{
       'API-KEY': 'a5008e7c-13ed-4c09-91fb-8786d1dab1cb'
    }

});
 


export const usersApi = {

    getUsers:(currentPage = 1,pageSize = 10) => {

        return instance.get(`users?page=${currentPage}&count=${pageSize}`,{ 
            withCredentials: true
        }).then( response=>{
            return response.data;
        } );
           
    
    },

    follow: (userId = 1)=>{
        return instance.post(`follow/${userId}`).then( response=>{
            return response.data;
        } );

    },
    unfollow:(userId = 1)=>{
        return instance.delete(`follow/${userId}`).then( response=>{
            return response.data;
        } );

    },

    getProfile:(profileId = 2)=>{
       
        return  instance.get(`profile/${profileId}`).then( response=>{
            return response.data;
        } );
    },


    me:() =>{

        return instance.get(`auth/me` );


    },


    




} 

