import {createStore, combineReducers, applyMiddleware} from "redux";
import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import usersReducer from "./users-reducer";
import authReducer from "./auth-reducer";

import provaReducer from "./provaReducer";

//import middelware layer for my thunk function 
import thunkMiddleware from "redux-thunk";

let reducers = combineReducers(
    {
        profilePage: profileReducer,
        dialogsPage:dialogsReducer,
        usersPage:usersReducer, 
        auth: authReducer,
        prova:provaReducer,
    }
);
// applyMiddleware si crea middle layer tra dispatch e reducer
let store = createStore(reducers,applyMiddleware( thunkMiddleware));
window.store = store;

export default store;