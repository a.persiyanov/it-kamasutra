import { usersApi } from '../api/api.js';
const FOLLOW = 'FOLLOW';
const UNFOLLOW = 'UNFOLLOW';
const SET_USERS = 'SET_USERS';
const SET_CURRENT_PAGE = 'SET_CURRENT_PAGE';
const SET_TOTAL_USERS = 'SET_TOTAL_USERS';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'TOGGLE_IS_FOLLOWING_PROGRESS';


let initialState = {
    users:[],
    pageSize:10,
    totalUsersCount:0,
    currentPage:1,
    isFetching: false,
    followingInProgress:[],
  
}



const usersReducer = (state = initialState, action)=>{
   
    switch(action.type){
        
        case FOLLOW:{
           
           return {
              
                ...state,
                users:state.users.map(function(u){
                  
                    if(u.id === action.userId){
                        return {...u, followed:true} ;
                    }
                    return u;
                }) 
            };

             
            
         }
        case UNFOLLOW :{
            return {
                ...state,
                users:state.users.map(function(u){
                    if(u.id === action.userId){
                        return {...u, followed:false} ;
                    }
                    return u;
                }) 
            };

        }


        case SET_USERS:{
         
            let u = {...state, users:action.users };
            return u;
        }
        case SET_CURRENT_PAGE:{
            // alert(action.currentPage);
           return  {...state, currentPage:action.currentPage };
           
        }

        case SET_TOTAL_USERS:{
         
           return {...state, totalUsersCount:action.totalUsersCount };
           
        }
        case TOGGLE_IS_FETCHING:{

            return {...state, isFetching:action.isFetching };
        }
        case TOGGLE_IS_FOLLOWING_PROGRESS:{
            return {
                ...state,
                followingInProgress: action.isFetching ?
                [ ...state.followingInProgress, action.userId]
                : state.followingInProgress.filter(id => id !=action.userId)
            } ;
        } 
       
        default : 
        return state;
    }

    




   
}
//Actions functions - tornano action dopo dispather
export const follow = (userId) =>{
    return {
        type: FOLLOW,userId,
    }
}
  
export const unfollow = (userId) =>{
    return {
        type: UNFOLLOW,userId,
    }
}
export const setUsers = (users) =>{
    return {
        type: SET_USERS,users,
    }
}
export const setCurrentPage = (currentPage) =>{
    return {
        type: SET_CURRENT_PAGE,currentPage,
    }
}
export const setTotalUsers=(totalUsersCount)=>{
    return {
        type: SET_TOTAL_USERS,totalUsersCount,
    }
}
export const toggleIsFetching=(isFetching)=>{
    return {
        type: TOGGLE_IS_FETCHING,isFetching,
    }
}

export const toggleFollowingProgress=(isFetching,userId)=>{
    return {
        type: TOGGLE_IS_FOLLOWING_PROGRESS,isFetching,userId,
    }
}


// ThunksCreators
export const getUsers = (currentPage,pageSize)=>{

    return (dispatch)=> {

        dispatch(toggleIsFetching(true));
        
        usersApi.getUsers(currentPage,pageSize).then(data => {
            dispatch(toggleIsFetching(false));
            dispatch(setUsers(data.items));
            dispatch(setTotalUsers(data.totalCount));
            dispatch(setCurrentPage(currentPage));
        });
    };

} 

export const following = (userId)=>{

    return (dispatch)=> {

        dispatch( toggleFollowingProgress(true,userId));

        usersApi.follow(userId).then(data=>{
            if(data.resultCode == 0 ){
             
                dispatch(follow(userId));
                  
            }
            dispatch(toggleFollowingProgress(false,userId));
         
           
           
           
         });

        

    }
}
export const unfollowing = (userId)=>{

    return (dispatch)=> {

        dispatch (toggleFollowingProgress(true,userId));
                    
        usersApi.unfollow(userId).then(data => {
         
            if(data.resultCode == 0 ){
             
                dispatch (unfollow(userId));
             
   
            }
            dispatch(toggleFollowingProgress(false,userId));
       
        });
    }
}

export default usersReducer;