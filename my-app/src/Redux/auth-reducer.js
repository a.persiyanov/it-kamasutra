import { usersApi } from '../api/api.js';
const TOGGLE_IS_FETCHING = 'TOGGLE_IS_FETCHING';
const SET_USER_DATA = 'SET_USERS_DATA';


let initialState = {
    userId: null,
    email: null,
    login: null,
    isAuth: false
  
}



const authReducer = (state = initialState, action)=>{
   
    switch(action.type){
        
        case SET_USER_DATA:{
         
            let u = {
                ...state,
                ...action.data ,
                isAuth:true
             };
           
            return u;
        }
        
        case TOGGLE_IS_FETCHING:{

            return {...state, isFetching:action.isFetching };
        }
       
        default : 
        return state;
    }

    




   
}


export const setAuthUserData = (userId,email,login) =>{
    return {
        type: SET_USER_DATA,data:{
            userId,email,login

        },
    }
}

export const toggleIsFetching=(isFetching)=>{
    return {
        type: TOGGLE_IS_FETCHING,isFetching,
    }
}


// ThunksCreators

export const authMe = ()=>{

    return (dispatch)=> {
   
        usersApi.me().then(response => {
         
            if(response.data.resultCode === 0 ){
              
                let {id:userId,login,email} = response.data.data;

                dispatch(setAuthUserData(userId,login,email)); 
                

            }
            
        });
    }
}

export default authReducer;