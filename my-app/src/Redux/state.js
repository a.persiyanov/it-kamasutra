import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";

const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';
const SEND_MESSAGE = 'SEND-MESSAGE';
const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';



let store = {
  //*_ non usarli
  _state:{

    profilePage:{
      posts:[
        {id:1, name:'Alex', message:'Hy it s  my first post.', likesCount:12},
        {id:2, name:'Alex', message:'Lets Go!!', likesCount:5},
        {id:3, name:'Alex', message:'bla bla', likesCount:5},
        {id:4, name:'Alex', message:'bla bla ssdfs', likesCount:5},
        {id:5, name:'Alex', message:'bla bldddddda', likesCount:5}
      ],
      newPostText:"text is here",
  
    },

    dialogsPage:{
     
      dialogs:[
        {id:1,name:'Alex'},
        {id:2,name:'Giacomo'},
        {id:3,name:'Pietro'},
        {id:4,name:'Elena'},
        {id:5,name:'Luca'},
        {id:6,name:'Julia'},
      ],
      messages:[
        {id: 1, message:'Hello!'},
        {id:2,message: 'Hi'},
        {id:3,message:'How are you?'},
        {id:4,message:'Fine and you?'},
        {id:5,message:'Hi'},
        {id:6,message:'Hello'}
    
      ],
      newMessageBody:"msg text",

    },
  },
  
  _callSubscriber(){
    console.dir("state changed");
  },
  //interface metodo per app (API)
  getState(){
    return this._state;
  },

  subscribe(observer){
   
    this._callSubscriber  = observer;
  
  },
  
  dispatch(action){
    profileReducer(this._state.profilePage, action);
    dialogsReducer(this._state.dialogsPage, action);
  
    this._callSubscriber(this._state);

  }

  

  
  


}










  
export default store;
window.store = store;