const UPDATE_NEW_MESSAGE_BODY = 'UPDATE-NEW-MESSAGE-BODY';
const SEND_MESSAGE = 'SEND-MESSAGE';


let initialState = {
    
    dialogs:[
        {id:1,name:'Alex'},
        {id:2,name:'Giacomo'},
        {id:3,name:'Pietro'},
        {id:4,name:'Elena'},
        {id:5,name:'Luca'},
        {id:6,name:'Julia'},
    ],
    messages:[
    {id: 1, message:'Hello!'},
    {id:2,message: 'Hi'},
    {id:3,message:'How are you?'},
    {id:4,message:'Fine and you?'},
    {id:5,message:'Hi'},
    {id:6,message:'Hello'}

    ],
    newMessageBody:"msg text",
}

export const sendMessageActionCreator = () =>{
    return {
        type: SEND_MESSAGE,
    }
}
  
export const updateNewMessageBodyCreator = (body) =>{
    return {
        type: UPDATE_NEW_MESSAGE_BODY, body:body,
    }
}




const dialogsReducer = (state = initialState, action)=>{



    switch(action.type){
        case UPDATE_NEW_MESSAGE_BODY:{

            let stateCopy = {...state};
            stateCopy.newMessageBody = action.body;
          
            return stateCopy;
        }
        case SEND_MESSAGE:{
            let stateCopy = {...state};
            
            let body = state.newMessageBody;
    
            stateCopy.messages.push({id:'7', message:body,});
            stateCopy.newMessageBody = '';
            return stateCopy;
        }
        default:
            return state;


    }
   

    
}
export default dialogsReducer;