import { usersApi } from '../api/api.js';
const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
const SET_USER_PROFILE = 'SET_USER_PROFILE';



let initialState = {
    posts:[
        {id:1, name:'Alex', message:'Hy it s  my first post.', likesCount:12},
        {id:2, name:'Alex', message:'Lets Go!!', likesCount:5},
        {id:3, name:'Alex', message:'bla bla', likesCount:5},
        {id:4, name:'Alex', message:'bla bla ssdfs', likesCount:5},
        {id:5, name:'Alex', message:'bla bldddddda111s', likesCount:5}
      ],
    newPostText:"text is here",
    profile:null,
}



const profileReducer = (state = initialState, action)=>{
   
    switch(action.type){
        
        case ADD_POST:{
           
            let newPost = {
                id:7,
                name:"name",
                message:state.newPostText,
                likesCount:0
                }
            
            let stateCopy = {...state};
            stateCopy.posts = [...state.posts];
            stateCopy.posts.push(newPost);
            stateCopy.newPostText ='';
           
            return stateCopy;
         }
        case UPDATE_NEW_POST_TEXT :{
            
            let stateCopy = {...state};
            stateCopy.newPostText = action.text;
            return stateCopy;
        }
        case SET_USER_PROFILE :{

                return {...state,profile:action.profile}
        }
        default :
        console.dir("stateCopy");
            return state;
    }

    




   
}

export const addPostActionCreator = () =>{
    return {
        type: ADD_POST,
    }
}
  
export const updateNewPostTextCreator = (text) =>{
    return {
        type: UPDATE_NEW_POST_TEXT,text:text
    }
}

export const setUserProfile = (profile) => {
    return{
        type: SET_USER_PROFILE,profile: profile
    }
}


// ThunksCreators

export const getProfile = (userId)=>{
    if(!userId){
        userId = 2;
    }
    return (dispatch)=> {
        usersApi.getProfile(userId).then( response=> {   return dispatch(setUserProfile(response)); } );
    }
}

export default profileReducer;