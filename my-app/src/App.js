import React from 'react';



import './App.css';
import HeaderContainer from './components/Header/HeaderContainer.jsx';
import Nav from './components/Navbar/Nav.jsx';
import ProfileContainer from './components/Profile/ProfileContainer.jsx';
import DialogsContainer from './components/Dialogs/DialogsContainer.jsx';
import News from './components/News/News.jsx';
import UsersContainer from './components/Users/UsersContainer.jsx';
import Login from './components/Login/Login.jsx';
import ProvaContainer from './components/prova/provaContainer.jsx';
import {Route} from "react-router-dom";




function App(props) {

  // let posts = props.state.profilePage;
  // let dialogsPage = props.state.dialogsPage;

  

  
  return (
  
      <div className="app-wrapper">
        <HeaderContainer/>
        <Nav/>
    
        <div className="app-wrapper-content">
        
          <Route  path="/profile/:userId?/:seond?"  render={()=> <ProfileContainer/> }/>
          <Route  path="/dialogs" render={()=> <DialogsContainer/>}/>

          {/* <Route  path="/dialogs" render={()=> <Dialogs dispatch={props.dispatch} state={props.state.dialogsPage} />}/> */}
          <Route  path="/news" render={()=><News/>}/>
          <Route  path="/users" render={()=><UsersContainer/>}/>
          <Route  path="/login" render={()=><Login/>}/>

          <Route  path="/prova" render={()=><ProvaContainer/>}/>
       
        
        </div>
        
    
     
    
   
   
      </div>
  
  );
}

export default App;
