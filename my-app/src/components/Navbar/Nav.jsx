import React from 'react';
import s from './Nav.module.css';

import {NavLink} from "react-router-dom";

 function Nav (){
    return(
        <nav className={s.nav}>
         
        <div className={`${s.item} ${s.active}`}>
         
          <NavLink to="/profile" activeClassName={s.active}>Profile</NavLink>

        </div>
        <div className={s.item}>
       
          <NavLink to="/dialogs" activeClassName={s.active}>Message</NavLink>
        </div>
        <div className={s.item}>
         <a href="/news">News(refresh page)</a>
        </div>
       
        <div className={s.item}>
          <NavLink to="/users" activeClassName={s.active}>Users</NavLink>
        </div>

        <div className={s.item}>
          <NavLink to="/prova" activeClassName={s.active}>Prova*</NavLink>
        </div>
        
      </nav>
    );
}
export default Nav;