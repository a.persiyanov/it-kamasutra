import React from 'react';
import store from '../../Redux/redux-store';
import Prova from  "./provaComponent";
import {connect} from "react-redux";
import {changeValueAction} from '../../Redux/provaReducer';
// function ProvaContainer (props){
  
//     return (
//     <Prova data={store.getState().prova.data}/> 
//         );
// }

const mapStateToProps = (state)=>{
    return {
        data:state.prova.data,
    }
}

const mapDispatchToProps = (dispatch)=>{
    return{
        changeValue: (text)=>{dispatch(changeValueAction(text))},
    }
}

export default connect (mapStateToProps,mapDispatchToProps)(Prova);