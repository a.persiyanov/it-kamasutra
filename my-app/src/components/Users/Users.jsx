import React from 'react';
import User from './User/User.jsx';
// import s from './Users.module.css';
import Pagination from './../common/Pagination/Pagination.jsx'
let Users = (props)=>{
    
    let pageCount = Math.ceil(props.totalUsersCount / props.pageSize);
    let pages = [];
   
    for(let i=1; i<=pageCount;i++){
        pages.push(i);
    }

  
        
    let usersElements  = props.users.map((user,i)=>{
        return(
            <User 
                id={user.id} 
                followed={user.followed} 
                name={user.name} 
                location={user.location} 
                status={user.status} 
                photoUrl={user.photos.small} 
                followingInProgress={props.followingInProgress}
                unfollowing = {props.unfollowing}
                following = {props.following}
                key={i}
            />
        );
    });
    
    return (
    
        <div>
            <p>Users</p> 
            <p>{props.pageSize} users from: {props.totalUsersCount}</p>
            <p>Current page: {props.currentPage}</p>
            
            <hr/>
            {usersElements}
            <Pagination pages={pages} onPageChange={props.onPageChange} currentPage={props.currentPage}/>
           
            
        </div>
    );

}

export default Users;
