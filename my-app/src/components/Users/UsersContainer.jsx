
import { setCurrentPage,getUsers,unfollowing,following } from '../../Redux/users-reducer.js';
import UsersApiComponent from './UsersApiComponent.jsx';
import {connect} from 'react-redux';



let mapStateToProps = (state) =>{
   return {
      users: state.usersPage.users,
      pageSize: state.usersPage.pageSize,
      totalUsersCount: state.usersPage.totalUsersCount,
      currentPage:state.usersPage.currentPage, 
      isFetching:state.usersPage.isFetching,
      followingInProgress:state.usersPage.followingInProgress,
      
      
      
   }
}


//scrivee in questo modo ugualle scrivere : follow: follow
//si torna object e non function  connect da solo wrappa con dispatch
let mapDispatchToProps  = {
   setCurrentPage,
   getUsers,
   unfollowing,
   following,
};


export default connect(mapStateToProps,mapDispatchToProps)(UsersApiComponent);


