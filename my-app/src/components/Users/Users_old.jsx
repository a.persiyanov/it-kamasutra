import React from 'react';
import s from './Users.module.css';
import User from './User/User.jsx';
import * as axios  from 'axios';

function Users (props){
    console.dir(props); 

    function getUsers(){

        if(props.users.length === 0){
        
            axios.get("https://social-network.samuraijs.com/api/1.0/users").then(response=>{
                
                props.setUsers(response.data.items);
            });
        
            // props.setUsers([
            //     {id:1, photoUrl:"https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png", followed:false, fullName:"Alex P", status:"My status......",location:{city:"Kaliningrad",country:"Russia",}},
            //     {id:2, photoUrl:"https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png", followed:true, fullName:"Ludovic K", status:"My status L.K",location:{city:"Gamburg",country:"Germany",}},
            //     {id:3, photoUrl:"https://cdn3.iconfinder.com/data/icons/avatars-15/64/_Ninja-2-512.png", followed:false, fullName:"David S", status:"My status D.S",location:{city:"Los Angeles",country:"USA",}},
            // ]);
        }


    }   
 
   

    let usersElements = props.users.map((user,i)=>{
        return <User id={user.id} followed={user.followed} name={user.name} location={user.location} status={user.status} photoUrl={user.photos.small} onFollow={props.onFollow} onUnfollow={props.onUnfollow} key={i}/>;
    });

    return(
   
    <div>
         Usesrs
         <button onClick={getUsers}>getUsers</button>
         <hr/>
      { usersElements}
      
       
    </div>
      
    );
}
export default Users;