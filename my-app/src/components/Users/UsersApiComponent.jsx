import React from 'react';
import Users from './Users.jsx';
import Preloader from '../common/Preloader/Preloader.jsx';

// import s from './UsersApiComponent.module.css';



class UsersApiComponent extends React.Component {
    
    componentDidMount(){
        //bll thunk 
        this.props.getUsers(this.props.currentPage,this.props.pageSize);
    }
   
    onPageChange = (page) => {
         //bll thunk 
       
        this.props.getUsers(page,this.props.pageSize);

      
    }

    componentWillUnmount(){
        // alert("WillUnmount");
    }

    render(){
       
       return (
           <div> 
               
               <Preloader isFetching={this.props.isFetching}/>
                <Users 
                users = {this.props.users} 
                currentPage={this.props.currentPage} 
                totalUsersCount={this.props.totalUsersCount}  
                pageSize = {this.props.pageSize}  
                onPageChange={this.onPageChange} 
               
                followingInProgress={this.props.followingInProgress}
               
                unfollowing={this.props.unfollowing}
                following={this.props.following}
                    
                />
                
           
           </div>
          
        
       );
     
 
    }
  
}
export default  UsersApiComponent;