import React from 'react';
import s from './User.module.css'
import avatarDefault from '../../../images/ninja_default.png';
import { NavLink } from 'react-router-dom';




const User = (props)=>{
   
    // let onFollow = ()=>{
    
    // }
    // let onUnfollow = ()=>{
    
    // }

    return ( 
        <div className={s.userContainer} >
            
            <NavLink to={'/profile/' + props.id}>
                 <img width="50px" src={props.photoUrl? props.photoUrl :avatarDefault} alt=""/> 
            </NavLink>
            <div className={s.id}>{props.id}</div>
            <div className={s.name}>{props.name}</div>
            <div> { props.followed ?
                 <button disabled={ props.followingInProgress.some( id => id === props.id) } onClick={ ()=>{
                     //.ui->bll--thunk 
                    props.unfollowing(props.id);
                 }
                    

                 }>unfollowed</button>:   
                 
                 <button disabled={ props.followingInProgress.some( id => id === props.id) } onClick={ ()=>{
                     //.ui->bll--thunk 
                    props.following(props.id);
                 }
                    


                 }>follow</button>} </div>
            <div>{props.status ? props.status: "Status null" }</div>
          
        </div>
    );
}

export default User;