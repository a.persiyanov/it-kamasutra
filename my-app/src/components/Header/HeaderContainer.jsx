
import React from 'react';


import Header from './Header';

import {connect} from 'react-redux';
import {authMe} from './../../Redux/auth-reducer.js';
import { withRouter } from "react-router";

 class HeaderContainer extends React.Component {

  



  componentDidMount(){
 
    this.props.authMe();
    
  }
  
  
  render (){
    return(
      
      
        <Header { ...this.props }/>
      
     
    );
  }
   
}
let mapStateToProps = (state) =>({
    isAuth: state.auth.isAuth,
    login: state.auth.userId
 
});

let mapDispatchToProps  = {authMe};

let withUrlDataContainerComponent = withRouter(HeaderContainer);

export default connect (mapStateToProps,mapDispatchToProps) (withUrlDataContainerComponent);