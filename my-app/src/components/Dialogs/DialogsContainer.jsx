import { updateNewMessageBodyCreator,sendMessageActionCreator   } from '../../Redux/dialogs-reducer.js';
import {connect} from 'react-redux';
import Dialogs from './Dialogs.jsx';


  
//Container Component React-Redux


let mapStateToProps = (state) =>{
    return {
        dialogs: state.dialogsPage.dialogs,
        messages:state.dialogsPage.messages,
        newMessageBody:state.dialogsPage.newMessageBody,
        isAuth: state.auth.isAuth,
      
    }
}

let mapDispatchToProps = (dispatch) =>{
    return {
        onUpdateMessage:(text)=>{ dispatch(updateNewMessageBodyCreator(text)); },
        onSendMessage:()=>{ dispatch(sendMessageActionCreator()); },
    }
}

const  SuperDialogsContainer = connect(mapStateToProps,mapDispatchToProps)(Dialogs);

export default SuperDialogsContainer;