import React from 'react';
import s from './Dialogs.module.css';
import Dialog from './Dialog/Dialog.jsx';
import Message from './Message/Message.jsx';
import {Redirect} from 'react-router-dom'; 

 function Dialogs (props){


   let messageData = props.messages;
   let dialogData = props.dialogs;
   let msgBody = props.newMessageBody;

   let dailogElements = createDialogElements();
   let messageElements = messageData.map( (message)=> <Message text={message.message} key={message.id}/>);
   let newMessageElement = React.createRef();
   
   

   function createDialogElements(){
    
    let elements = dialogData.map(
      (dialog,i)=>{
        return  <Dialog id={dialog.id} name={dialog.name} key={dialog.id} /> ;
      }
    );

    return elements;
   }
  

   function onChangeMyMessage(e){
    let text = e.target.value;
    props.onUpdateMessage(text);
    
   }

   function onAddMessage(){
    props.onSendMessage();
   }

   function onRemoveMessage(){
      alert("remove");
   }

   if(!props.isAuth){ 
     return <Redirect to={"/login"}/>

   }

    return(
      
      <div className={s.dialogs}>
      
        <div className={s.dialogs_items}> 
        
       
          {dailogElements}
          
        </div>
        <div className={s.messages}>
          
          {messageElements}
        </div>
        <div className={s.addForm}>
   
          <textarea onChange={onChangeMyMessage} ref={newMessageElement} value={msgBody} ></textarea><br/>
          <button onClick={onAddMessage} >Add Message</button>
          <button onClick={onRemoveMessage}>Remove!!</button>

        </div>
      </div>
    );
}
export default Dialogs;