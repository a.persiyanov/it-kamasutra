import React from 'react';
import s from './Preloader.module.css'
import loading from '../../../images/loading.gif';

let Preloader = (props)=>{
   
 
    return ( 
        <div className={ `${s.loadingContainer} ${props.isFetching ?s.active:""}`  } >
            {/* {props.isFetching ? <img className={s.loading} src={loading} alt=""/> : null }  */}
           <img className={s.loading} src={loading} alt=""/> 
        </div>
    );
}

export default Preloader;