import React from 'react';
import s from './Pagination.module.css'
import loading from '../../../images/loading.gif';

let Pagination = (props)=>{
   
 
    return ( 
        <div className={s.pagination}>

            <hr/>
            <div className={s.paginationContainer}>
                current page : {props.currentPage} <br/>
            {props.pages.map( 
                (p,i)=>{ 
                    return (
                       
                             <div onClick={()=>(props.onPageChange(p))} className={s.pageItem + " " +(props.currentPage===p ? s.currentPage : "" ) } key={i}> <span>{p}</span>  </div>
                       
                       
                    ); 
                }
            )}
             </div>
            
            
        </div>
       
    );
}

export default Pagination;