// import s from './MyPostsContainer.module.css';
import { addPostActionCreator, updateNewPostTextCreator } from '../../../Redux/profile-reducer.js';
import MyPosts from './MyPosts.jsx';
import {connect} from 'react-redux';


let mapStateToProps = (state) =>{
   return {
      posts: state.profilePage.posts,
      newPostText:state.profilePage.newPostText,
      
   }
}

let mapDispatchToProps = (dispatch) =>{
   return {
      addPost:()=>{ dispatch( addPostActionCreator() ); },
      updateNewPostText:(text)=>{ dispatch( updateNewPostTextCreator(text) ); },
   }
}

const  MyPostsContainer = connect(mapStateToProps,mapDispatchToProps)(MyPosts);

export default MyPostsContainer;


