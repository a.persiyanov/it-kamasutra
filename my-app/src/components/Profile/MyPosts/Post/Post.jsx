
import React from 'react';
import s from './Post.module.css';

 function Post (props){
     
    return(
   
    <div className={s.item}>
       
        <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png" alt="avatar"/>
        {props.name} {props.message}  {props.likesCount}
       
    </div>
      
    );
}
export default Post;