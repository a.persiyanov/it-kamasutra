
import React from 'react';
import Post from './Post/Post.jsx';
import s from './MyPosts.module.css';




 function MyPosts (props){

  let  posts = props.posts;

   
  let newPostElement = React.createRef();




  let postsElements = posts.map((post,i)=> <Post id={post.id} name={post.name} message={post.message} likesCount={post.likesCount}  key={i} />)

  let onAddPost = ()=>{
    props.addPost();
   
  }

  let onChangeMyPost = ()=>{
    
    let text = newPostElement.current.value;
    props.updateNewPostText(text);
   
   
  }

  let onRemovePost = ()=>{
    alert("Remove post");
  }
    return(
    <div className={s.container}>
      My posts
      <div className={s.addForm}>
   
        <textarea onChange={onChangeMyPost} ref={newPostElement} value={props.newPostText}></textarea>
        <button onClick={onAddPost} >Add post</button>
        <button onClick={onRemovePost}>Remove!!</button>
      </div>
      
     
      <div className={s.posts}>
       {postsElements}
      </div>
    </div>
    );
}
export default MyPosts;