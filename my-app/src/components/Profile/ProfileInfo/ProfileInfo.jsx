
import React from 'react';
import s from './ProfileInfo.module.css';
import Preloader from '../../common/Preloader/Preloader';



 function ProfileInfo (props){
    console.dir(props);

    if(!props.profile){
        return  <Preloader isFetching={true}/>
    }
  
    return(
        <div className={s.container}>
            {/* <img src="https://www.gettyimages.pt/gi-resources/images/Homepage/Hero/PT/PT_hero_42_153645159.jpg" alt="img"/> */}
            <div className={s.backgroundImg}></div>
            <div className={s.userInfoContainer}>
                <div className={s.item}><img src={props.profile.photos.large} alt=""/></div>
                 
                
               <div className={s.item}>  
                    <div className={s.label}>
                        Full Name
                    </div>
                    <div>
                        {props.profile.fullName}
                    </div>
                    
                </div>
               <div className={s.item}>
               <div className={s.label}>
                        About Me
                    </div>
                    <div>
                        {props.profile.aboutMe}
                    </div>
                </div>
               <div className={s.item}>
               <div className={s.label}>
                        Looking For a Job Description
                    </div>
                    <div>
                        {props.profile.lookingForAJobDescription}
                    </div>
                </div>
               
               
            </div>
            
          
       </div>
    );
}
export default ProfileInfo;