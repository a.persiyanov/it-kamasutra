
import React from 'react';


import Profile from './Profile';
import * as axios  from 'axios';
import {connect} from 'react-redux';
import {getProfile} from './../../Redux/profile-reducer.js';
import { withRouter } from "react-router";

 class ProfileContainer extends React.Component {

  


  //tutti siderEffects si fanno qua
  componentDidMount(){
  //  debugger;
  let userId = this.props.match.params.userId;
  // if(!userId){
  //     userId = 2;
  // }
  //   axios.get(`https://social-network.samuraijs.com/api/1.0/profile/${userId}`).then(response=>{
  //     // console.dir(response.data);
  //       this.props.setUserProfile(response.data);
      
  //     ;
      
  //   });
  
  
  //thunk function
  this.props.getProfile(userId);
    
  }
  
  
  render (){
    return(
      
      <div>
        <Profile  {...this.props} profile={this.props.profile} />
      
      
      </div>
        
        
        
     
    );
  }
   
}
let mapStateToProps = (state) =>({
  profile:state.profilePage.profile,
});

let mapDispatchToProps  = {getProfile};

let withUrlDataContainerComponent = withRouter(ProfileContainer);

export default connect (mapStateToProps,mapDispatchToProps) (withUrlDataContainerComponent);